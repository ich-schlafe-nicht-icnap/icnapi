table! {
    machine_status (id) {
        id -> Int4,
        machine_id -> Int8,
        timestamp -> Timestamp,
        event_count -> Int8,
        run -> Nullable<Bool>,
        auto -> Nullable<Bool>,
        error -> Nullable<Bool>,
    }
}

table! {
    operator_reason (id) {
        id -> Int4,
        machine_id -> Int8,
        timestamp -> Timestamp,
        reason -> Text,
        description -> Nullable<Text>,
        error_time -> Timestamp,
        operator -> Text,
        resolution -> Nullable<Text>,
        review -> Nullable<Int2>,
    }
}

allow_tables_to_appear_in_same_query!(
    machine_status,
    operator_reason,
);

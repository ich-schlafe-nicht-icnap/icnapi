#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_migrations;
#[macro_use] extern crate gotham_restful;
#[macro_use] extern crate log;

mod routing;
mod schema;
mod state;

use crate::state::ServerSharedState;
use diesel::{Connection, PgConnection};
use dotenv::dotenv;
use gotham::{
	middleware::{
		logger::RequestLogger,
		state::StateMiddleware
	},
	pipeline::{
		single::single_pipeline,
		new_pipeline
	},
	router::builder::build_router
};
use gotham_middleware_diesel::DieselMiddleware;
use log::LevelFilter;
use log4rs::{
	append::console::ConsoleAppender,
	config::{Appender, Config, Root},
	encode::pattern::PatternEncoder,
	Handle
};
use std::env;

fn init_logging() -> anyhow::Result<Handle>
{
	let stdout_encoder = PatternEncoder::new("{d(%Y-%m-%d %H:%M:%S%.3f %Z)} [{l}] {M} - {m}\n");
	
	let stdout = ConsoleAppender::builder()
		.encoder(Box::new(stdout_encoder))
		.build();
	
	let config = Config::builder()
		.appender(Appender::builder().build("stdout", Box::new(stdout)))
		.build(Root::builder().appender("stdout").build(LevelFilter::Info))?;
	
	Ok(log4rs::init_config(config)?)
}

embed_migrations!();

type Repo = gotham_middleware_diesel::Repo<PgConnection>;

/// Helper method to get a default database connection. It will read the `DATABASE_URL` environment
/// variable after running dotenv. The intended use is for small binaries that don't need a connection
/// pool. Do not use for the main backend.
fn default_pq_connection() -> PgConnection
{
	dotenv().ok();
	
	PgConnection::establish(
		&env::var("DATABASE_URL").expect("DATABASE_URL must be set")
	).expect("Unable to connect to database")
}

fn main() -> anyhow::Result<()>
{
	init_logging()?;
	
	let logging = RequestLogger::new(log::Level::Info);

	let state = StateMiddleware::new(ServerSharedState::default());

	let conn = default_pq_connection();
	embedded_migrations::run(&conn).expect("Unable to run database migrations");
	
	let dburl = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
	let repo = Repo::new(&dburl);
	let diesel = DieselMiddleware::new(repo);
	
	let (chain, pipelines) = single_pipeline(
		new_pipeline()
			.add(logging)
			.add(state)
			.add(diesel)
			.build()
	);
	
	let addr = "[::]:2298";
	gotham::start(addr, build_router(chain, pipelines, |route| {
		routing::routing(route)
	}));
	info!("Listening on {}", addr);
	Ok(())
}

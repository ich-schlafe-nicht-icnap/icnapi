use crate::{
	schema::machine_status,
	state::ServerSharedState,
	Repo
};
use chrono::NaiveDateTime;
use diesel::{dsl::insert_into, Insertable, RunQueryDsl};
use futures::future::Future;
use gotham::state::{FromState, State};
use gotham_restful::{NoContent, OpenapiSchema, Resource, ResourceResult, Response};
use hyper::StatusCode;
use serde::{Deserialize, Serialize};
use serde_json::Error as SerdeJsonError;

#[derive(Resource)]
#[rest_resource(update_all)]
pub struct StatusResource;

#[derive(Deserialize, Eq, Hash, OpenapiType, PartialEq, Serialize)]
pub enum StatusType
{
	Run,
	Auto,
	Error
}

#[derive(Deserialize, OpenapiType, Serialize)]
struct UpdateStatus
{
    machine_id : i64,
	ty : StatusType,
	status : bool
}

#[derive(Debug, Insertable)]
#[table_name = "machine_status"]
struct InsertStatus
{
    machine_id : i64,
	timestamp : NaiveDateTime,
	event_count : i64,
	run : Option<bool>,
	auto : Option<bool>,
	error : Option<bool>
}

struct RetryLater
{
	retry : bool
}

impl ResourceResult for RetryLater
{
	fn into_response(self) -> Result<Response, SerdeJsonError>
	{
		if !self.retry
		{
			return NoContent::default().into_response();
		}

		Ok(Response {
			body: Default::default(),
			mime: None,
			status: StatusCode::CONFLICT
		})
	}

	fn schema() -> OpenapiSchema
	{
		<NoContent as ResourceResult>::schema()
	}
}

#[rest_update_all(StatusResource)]
fn update_all(state : &mut State, body : UpdateStatus) -> RetryLater
{
	let repo = Repo::borrow_from(state);
	let state = &*ServerSharedState::borrow_from(state).0;

	{
		let state = &mut *state.write().unwrap();
        if !state.contains_key(&body.machine_id) {
            return RetryLater{ retry : true };
        }
		state.get_mut(&body.machine_id).unwrap().status.insert(body.ty, body.status);
	}

	{
		let state = &*state.read().unwrap();
        let state = state.get(&body.machine_id).unwrap();
		let datetime = match state.datetime {
			Some(datetime) => datetime,
			None => return RetryLater{ retry: true }
		};
		let event_count = match state.event_count {
			Some(count) => count,
			None => return RetryLater{ retry: true }
		};
		let ins = InsertStatus {
            machine_id: body.machine_id,
			timestamp: datetime,
			event_count,
			run: state.status.get(&StatusType::Run).map(|b| *b),
			auto: state.status.get(&StatusType::Auto).map(|b| *b),
			error: state.status.get(&StatusType::Error).map(|b| *b)
		};
		repo.run(move |conn| {
			insert_into(machine_status::table).values(ins).execute(&conn)
		}).wait().unwrap();
	}

	RetryLater { retry: false }
}

use gotham::{
	pipeline::chain::PipelineHandleChain,
	router::builder::*
};
use gotham_restful::{DrawResources, GetOpenapi, WithOpenapi};
use std::panic::RefUnwindSafe;

pub mod datetime;
use datetime::DateTimeResource;
pub mod eventcounter;
use eventcounter::EventCounterResource;
pub mod history;
use history::HistoryResource;
pub mod reason;
use reason::ReasonResource;
pub mod resolution;
use resolution::ResolutionResource;
pub mod review;
use review::ReviewResource;
pub mod status;
use status::StatusResource;

pub fn routing<C, P>(route : &mut RouterBuilder<C, P>)
where
	C: PipelineHandleChain<P> + Copy + Send + Sync + 'static,
	P: RefUnwindSafe + Send + Sync + 'static
{
	route.with_openapi("ICNAPI", "HIER GIBTS NIX ZU SEHEN", "http://localhost:2298", |mut route| {
		route.resource::<DateTimeResource, _>("datetime");
		route.resource::<EventCounterResource, _>("eventcounter");
		route.resource::<HistoryResource,_>("history");
                route.resource::<ReasonResource, _>("reason");
		route.resource::<ResolutionResource, _>("resolution");
		route.resource::<ReviewResource, _>("review");
		route.resource::<StatusResource, _>("status");
		
		route.get_openapi("openapi");
	});
}


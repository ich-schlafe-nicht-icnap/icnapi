use crate::state::ServerSharedState;
use chrono::NaiveDateTime;
use gotham::state::{FromState, State};
use gotham_restful::Resource;
use serde::{Deserialize, Serialize};

#[derive(Resource)]
#[rest_resource(create)]
pub struct DateTimeResource;

#[derive(Deserialize, OpenapiType, Serialize)]
struct NewDateTime
{
        machine_id: i64,
	datetime : NaiveDateTime
}

#[rest_create(DateTimeResource)]
fn create(state : &mut State, body : NewDateTime)
{
	let state = &*ServerSharedState::borrow_from(state).0;

	{
		let state = &mut *state.write().unwrap();
                if state.contains_key(&body.machine_id) {
                    state.get_mut(&body.machine_id).unwrap().datetime = Some(body.datetime);
                } else {
                    state.insert(body.machine_id, crate::state::ServerState{datetime: Some(body.datetime),..Default::default()});
                }
	}
}

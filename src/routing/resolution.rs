use crate::{
	schema::operator_reason,
	Repo
};
use diesel::{dsl::update, ExpressionMethods, QueryResult, RunQueryDsl};
use futures::future::Future;
use gotham::state::{FromState, State};
use gotham_restful::{NoContent, Resource};
use serde::{Deserialize, Serialize};

#[derive(Resource)]
#[rest_resource(create)]
pub struct ResolutionResource;

#[derive(Deserialize, OpenapiType, Serialize)]
struct NewResolution
{
	reason : i32,
	resolution : String
}

#[rest_create(ResolutionResource)]
fn create(state : &mut State, body : NewResolution) -> QueryResult<NoContent>
{
	let repo = Repo::borrow_from(state);
	repo.run(move |conn| {
		update(operator_reason::table)
			.filter(operator_reason::id.eq(&body.reason))
			.set(operator_reason::resolution.eq(&body.resolution))
			.execute(&conn)?;
		Ok(().into())
	}).wait()
}

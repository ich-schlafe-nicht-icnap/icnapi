use crate::{
	schema::operator_reason,
	Repo
};
use chrono::NaiveDateTime;
use diesel::{
	dsl::{insert_into, update},
	ExpressionMethods, Insertable, QueryResult, RunQueryDsl
};
use futures::future::Future;
use gotham::state::{FromState, State};
use gotham_restful::{NoContent, Resource};
use serde::{Deserialize, Serialize};

#[derive(Resource)]
#[rest_resource(read_all, create, update)]
pub struct ReasonResource;

#[derive(Deserialize, OpenapiType, Serialize)]
enum ReasonType
{
	TechnicalProblem,
	LogisticalProblem,
	ResourceProblem,
	OSI8,
	Unknown
}

#[derive(Deserialize, OpenapiType, Serialize)]
struct NewReason
{
	reason : ReasonType,
	description : Option<String>,
	error_time : NaiveDateTime,
	machine_id : i64,
	operator : String
}

#[derive(Deserialize, OpenapiType, Serialize)]
struct UpdateReason
{
	reason : ReasonType,
	description : Option<String>
}

#[derive(Deserialize, OpenapiType, Queryable, Serialize)]
struct Reason
{
	id : i32,
	machine_id : i64,
	timestamp : NaiveDateTime,
	reason : String,
	description : Option<String>,
	error_time : NaiveDateTime,
	operator : String,
	resolution : Option<String>,
	review : Option<i16>
}

#[derive(Insertable)]
#[table_name = "operator_reason"]
struct InsertReason
{
	reason : String,
	description : Option<String>,
	error_time : NaiveDateTime,
	operator : String,
	machine_id : i64
}

#[rest_read_all(ReasonResource)]
fn read_all(state : &mut State) -> QueryResult<Vec<Reason>>
{
	let repo = Repo::borrow_from(state);
	repo.run(move |conn| {
		operator_reason::table.load(&conn)
	}).wait()
}

#[rest_create(ReasonResource)]
fn create(state : &mut State, body : NewReason) -> QueryResult<NoContent>
{
	let repo = Repo::borrow_from(state);
	
	let ins = InsertReason {
		reason: serde_json::to_string(&body.reason).unwrap(),
		description: body.description,
		error_time: body.error_time,
		machine_id: body.machine_id,
		operator: body.operator
	};
	repo.run(move |conn| {
		insert_into(operator_reason::table).values(ins).execute(&conn)?;
		Ok(().into())
	}).wait()
}

#[rest_update(ReasonResource)]
fn update(state : &mut State, id : i32, body : UpdateReason) -> QueryResult<NoContent>
{
	let repo = Repo::borrow_from(state);
	repo.run(move |conn| {
		update(operator_reason::table)
			.filter(operator_reason::id.eq(id))
			.set((
				operator_reason::reason.eq(serde_json::to_string(&body.reason).unwrap()),
				operator_reason::description.eq(body.description)
			))
			.execute(&conn)?;
		Ok(().into())
	}).wait()
}

use chrono::{NaiveDate, NaiveDateTime};
use crate::diesel::{ExpressionMethods, query_dsl::filter_dsl::FilterDsl};
use crate::diesel::RunQueryDsl;
use crate::diesel::query_dsl::methods::OrderDsl;
use futures::future::Future;
use gotham::state::{FromState, State};
use gotham_restful::Resource;
use serde::{Deserialize, Serialize};
use std::ops::Add;

#[derive(Resource)]
#[rest_resource(read)]
pub struct HistoryResource;

#[derive(Deserialize, OpenapiType, Serialize)]
struct History
{
    datetime: NaiveDateTime,
    stopped_machines: i64,
    broken_machines: i64,
}


impl From<(NaiveDateTime,bool,bool)> for History {
    fn from((datetime, stopped, broken): (NaiveDateTime, bool, bool)) -> Self {
        let stopped_machines = if stopped {
            1
        } else {
            0
        };
        let broken_machines = if broken {
            1
        } else {
            0
        };
        Self{datetime, stopped_machines, broken_machines}
    }
}

#[rest_read(HistoryResource)]
fn read(state : &mut State, date: NaiveDate) -> diesel::QueryResult<Vec<History>>
{
    use crate::Repo;
    use crate::schema::machine_status::dsl::*;
    let repo = Repo::borrow_from(state);
    repo.run(move|con|{
        let mut stopped = Vec::new();
        let mut broken = Vec::new();
        let mut recent_broken = None;
        let mut recent_stoped = None;
        let machine_stati = machine_status.filter(diesel::dsl::date(timestamp).eq(date)).order(timestamp.asc()).load::<MachineStatus>(&con)?; 
        machine_stati.iter().filter(|ms| ms.run.is_some() || ms.error.is_some()).for_each(|ms| {
            match ms.error {
                Some(true) => {
                   match recent_broken {
                        Some(ts) => {
                            broken.push({(ts, ms.timestamp)});
                        },
                        None => {
                            broken.push({(date.and_hms(0,0,0), ms.timestamp)});
                        },
                   }
                },
                Some(false) => {
                    recent_broken = Some(ms.timestamp);
                },
                None => {},
            }
            match ms.run {
                Some(true) => {
                   match recent_stoped {
                        Some(ts) => {
                            stopped.push({(ts, ms.timestamp)});
                        },
                        None => {
                            stopped.push({(date.and_hms(0,0,0), ms.timestamp)});
                        },
                   }
                },
                Some(false) => {
                    recent_stoped = Some(ms.timestamp);
                },
                None => {},
            }
        });
    if let Some(recent) = recent_broken {
        broken.push((recent, date.and_hms(23,59,59)));
    }
    if let Some(recent) = recent_stoped {
        stopped.push((recent, date.and_hms(23,59,59)));
    }
    let next_day = date.add(chrono::Duration::days(1)).and_hms(0, 0, 0);
    let mut ts = date.and_hms(0,0,0);
    let mut history = Vec::new();
    let mut stopped_iter = stopped.iter();
    let mut broken_iter = broken.iter();
    let (mut current_stopped_start, mut current_stopped_end) = stopped_iter.next().unwrap_or(&(next_day, next_day));
    let (mut current_broken_start, mut current_broken_end) = broken_iter.next().unwrap_or(&(next_day, next_day));
    while ts < next_day {
        let mut st = false;
        let mut br = false;
        match (current_stopped_start <= ts, current_stopped_end > ts) {
            (true, true) => st = true,
            (_,false) => {
                let (a, b) = *stopped_iter.next().unwrap_or(&(next_day, next_day));
                current_stopped_start = a;
                current_stopped_end = b;
            }
            _ => {},
        }
        match (current_broken_start <= ts, current_broken_end > ts) {
            (true, true) => br = true,
            (_,false) => {
                let (a, b) = *broken_iter.next().unwrap_or(&(next_day, next_day));
                current_broken_start = a;
                current_broken_end = b;
            },
            _ => {},
        }
        history.push((ts, st, br).into());
        ts = ts.add(chrono::Duration::hours(1));
    }

    Ok(history)
    }).wait()
}

#[derive(Queryable)]
struct MachineStatus {
    _id: i32,
    _machine_id: i64,
    timestamp: NaiveDateTime,
    _event_count: i64,
    run: Option<bool>,
    _auto: Option<bool>,
    error: Option<bool>,
}

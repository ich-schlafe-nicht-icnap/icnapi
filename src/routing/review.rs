use crate::{
	schema::operator_reason,
	Repo
};
use diesel::{dsl::update, ExpressionMethods, QueryResult, RunQueryDsl};
use futures::future::Future;
use gotham::state::{FromState, State};
use gotham_restful::{NoContent, Resource};
use serde::{Deserialize, Serialize};

#[derive(Resource)]
#[rest_resource(create)]
pub struct ReviewResource;

#[derive(Deserialize, OpenapiType, Serialize)]
struct NewReview
{
	reason : i32,
	review : i16
}

#[rest_create(ReviewResource)]
fn create(state : &mut State, body : NewReview) -> QueryResult<NoContent>
{
	let repo = Repo::borrow_from(state);
	repo.run(move |conn| {
		update(operator_reason::table)
			.filter(operator_reason::id.eq(&body.reason))
			.set(operator_reason::review.eq(&body.review))
			.execute(&conn)?;
		Ok(().into())
	}).wait()
}

use crate::state::ServerSharedState;
use gotham::state::{FromState, State};
use gotham_restful::Resource;
use serde::{Deserialize, Serialize};

#[derive(Resource)]
#[rest_resource(update_all)]
pub struct EventCounterResource;

#[derive(Deserialize, OpenapiType, Serialize)]
struct UpdateEventCounter
{
        machine_id: i64,
	count : i64
}

#[rest_update_all(EventCounterResource)]
fn update_all(state : &mut State, body : UpdateEventCounter)
{
	let state = &*ServerSharedState::borrow_from(state).0;
	
	{
		let state = &mut *state.write().unwrap();
		//state.event_count = Some(body.count);
                if state.contains_key(&body.machine_id) {
                    state.get_mut(&body.machine_id).unwrap().event_count = Some(body.count);
                } else {
                    state.insert(body.machine_id, crate::state::ServerState{event_count: Some(body.count), ..Default::default()});
                }
	}
}

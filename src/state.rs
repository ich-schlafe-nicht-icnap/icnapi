use crate::routing::status::StatusType;
use chrono::NaiveDateTime;
use gotham_derive::StateData;
use std::{
	collections::HashMap,
	sync::{Arc, RwLock}
};

#[derive(Default)]
pub struct ServerState
{
	pub datetime : Option<NaiveDateTime>,
	pub event_count : Option<i64>,
	pub status : HashMap<StatusType, bool>
}

#[derive(Clone, Default, StateData)]
pub struct ServerSharedState(pub Arc<RwLock<HashMap<i64,ServerState>>>);

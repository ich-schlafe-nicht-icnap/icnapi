CREATE TABLE machine_status (
    id SERIAL PRIMARY KEY,
    machine_id BIGINT NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    event_count BIGINT NOT NULL,
    run BOOLEAN,
    auto BOOLEAN,
    error BOOLEAN
);


CREATE TABLE operator_reason (
    id SERIAL PRIMARY KEY,
    machine_id BIGINT NOT NUll,
    timestamp TIMESTAMP NOT NULL DEFAULT NOW(),
    reason TEXT NOT NULL,
    description TEXT,
    error_time TIMESTAMP NOT NULL,
	operator TEXT NOT NULL,
	resolution TEXT,
	review INT2
);
